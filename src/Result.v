Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Inductive t (A : Set) : Set :=
| Ok (s : A)
| Error (e : string).
Arguments Ok {_}.
Arguments Error {_}.

Definition bind {A B : Set} (m : t A) (f : A -> t B) : t B:=
  match m with
  | Ok x => f x
  | Error e => Error e
  end.

Definition bind_prop {A : Set} (m : t A) (f : A -> Prop) : Prop :=
  match m with
  | Ok x => f x
  | Error e => True
  end.

Module Notations.
  Notation "'let?' A := B 'in' C" :=
    (bind B (fun A => C)) 
      (at level 200, A name, B at level 100, C at level 200).
  Notation "'let?' ' A := B 'in' C" :=
    (bind B (fun A => C)) 
      (at level 200, A pattern, B at level 100, C at level 200).
  Notation "'return?' A" := (Ok A) (at level 200).

  Notation "'letP?' ' A := B 'in' C" :=
    (bind_prop B (fun A => C))
      (at level 200, A pattern, B at level 100, C at level 200).
  Notation "'letP?' A := B 'in' C" :=
    (bind_prop B (fun A => C))
      (at level 200, A name, B at level 100, C at level 200).
End Notations.
