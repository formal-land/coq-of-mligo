(** File generated by coq-of-ocaml *)
Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require CoqOfMligo.Virtual.Big_map_mli.
Require CoqOfMligo.Virtual.Std.

Module Big_map_impl <: Big_map_mli.S.
  Definition big_map (key value : Set) : Set := list (key * value).

  Definition empty {key value : Set} : big_map key value := [].

  Definition literal {key value : Set}
    (arg : list (key * value)) : big_map key value := arg.

  Fixpoint find_opt {key value : Set}
    (k : key) (map : big_map key value) : option value :=
    match map with
    | [] => None
    | (k', v) :: tl =>
        if Std.polymorphic_eq k' k
        then Some v
        else find_opt k tl
    end.

  Definition mem {key value : Set}
    (k : key) (map : big_map key value) : bool :=
    match find_opt k map with
    | None => false
    | Some _ => true
    end.

  Fixpoint remove {key value : Set}
    (k : key) (map : big_map key value)
    : big_map key value :=
    match map with
    | [] => []
    | (k', _) as hd :: tl =>
        if Std.polymorphic_eq k' k
        then tl
        else hd :: (remove k tl)
    end.

  Definition update {key value : Set}
    (k : key) (v : option value) (map : big_map key value)
    : big_map key value :=
    match v with
    | Some v' => (k, v') :: (remove k map)
    | None => remove k map
    end.

  Definition add {key value : Set}
    (k : key) (v : value) (map : big_map key value)
    : big_map key value :=
    update k (Some v) map.

  Definition get_and_update {key value : Set}
    (k : key) (v : option value) (map : big_map key value)
    : option value * big_map key value :=
    let v2 := find_opt k map in
    let map' := remove k map in
    match v with
    | None => (v2, map')
    | Some v => (v2, (k, v) :: map')
    end.
End Big_map_impl.
Include Big_map_impl.
