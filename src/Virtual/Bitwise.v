(** File generated by coq-of-ocaml *)
Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require CoqOfMligo.Virtual.Bitwise_mli.
Require CoqOfMligo.Virtual.Std.

Module Bitwise_impl : Bitwise_mli.S.
  Parameter _and : Std.nat -> Std.nat -> Std.nat.

  Parameter _or : Std.nat -> Std.nat -> Std.nat.

  Parameter xor : Std.nat -> Std.nat -> Std.nat.

  Parameter shift_left : Std.nat -> Std.nat -> Std.nat.

  Parameter shift_right : Std.nat -> Std.nat -> Std.nat.
End Bitwise_impl.
Include Bitwise_impl.
