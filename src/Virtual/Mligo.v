(** File generated by coq-of-ocaml *)
Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require CoqOfMligo.Virtual.Mligo_mli.
Require CoqOfMligo.Virtual.Big_map.
Require CoqOfMligo.Virtual.Bitwise.
Require CoqOfMligo.Virtual.Bytes.
Require CoqOfMligo.Virtual.Crypto.
Require CoqOfMligo.Virtual.List.
Require CoqOfMligo.Virtual.Map.
Require CoqOfMligo.Virtual.Option.
Require CoqOfMligo.Virtual.Std.
Require CoqOfMligo.Virtual.String.
Require CoqOfMligo.Virtual.Test.
Require CoqOfMligo.Virtual.Tezos.
Require CoqOfMligo.Virtual._Set_mli. Module _Set := _Set_mli.

Module Mligo_impl : Mligo_mli.S.
  Parameter Included_S_chest_opening_result : Set.

  Parameter Included_S_operation : Set.

  Parameter Included_S_contract : Set -> Set -> Set.

  Parameter Included_S :
    Std.S (chest_opening_result := Included_S_chest_opening_result)
      (operation := Included_S_operation) (contract := Included_S_contract).

  Definition address := Included_S.(Std.S.address).

  Definition chain_id := Included_S.(Std.S.chain_id).

  Definition key := Included_S.(Std.S.key).

  Definition key_hash := Included_S.(Std.S.key_hash).

  Definition nat := Included_S.(Std.S.nat).

  Definition tez := Included_S.(Std.S.tez).

  Definition timestamp := Included_S.(Std.S.timestamp).

  Definition chest := Included_S.(Std.S.chest).

  Definition chest_key := Included_S.(Std.S.chest_key).

  Definition chest_opening_result := Included_S.(Std.S.chest_opening_result).

  Definition bls12_381_fr := Included_S.(Std.S.bls12_381_fr).

  Definition bls12_381_g1 := Included_S.(Std.S.bls12_381_g1).

  Definition bls12_381_g2 := Included_S.(Std.S.bls12_381_g2).

  Definition sapling_state := Included_S.(Std.S.sapling_state).

  Definition sapling_transaction := Included_S.(Std.S.sapling_transaction).

  Definition ticket := Included_S.(Std.S.ticket).

  Definition operation := Included_S.(Std.S.operation).

  Definition contract := Included_S.(Std.S.contract).

  Definition is_nat : int -> option nat := Included_S.(Std.S.is_nat).

  Definition abs : int -> nat := Included_S.(Std.S.abs).

  Definition int_value : nat -> int := Included_S.(Std.S.int_value).

  Definition unit_value : unit := Included_S.(Std.S.unit_value).

  Definition ediv : int -> int -> option (int * nat) := Included_S.(Std.S.ediv).

  Definition failwith {a : Set} : string -> a := Included_S.(Std.S.failwith).

  Definition failwith_int {a : Set} : int -> a := Included_S.(Std.S.failwith_int).

  Definition string : Set := String.string.

  Definition bytes : Set := Bytes.bytes.

  Definition list (a : Set) : Set := List.list a.

  Definition big_map (key value : Set) : Set := Big_map.big_map key value.

  Definition map (key value : Set) : Set := Map.map key value.

  Definition set (value : Set) : Set := _Set.set value.
End Mligo_impl.
Include Mligo_impl.
