(** File generated by coq-of-ocaml *)
Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require CoqOfMligo.Virtual._Set_mli.
Require CoqOfMligo.Virtual.List.
Require CoqOfMligo.Virtual.Std.

Module _Set_impl : _Set_mli.S.
  Definition set (value : Set) : Set := list value.

  Parameter empty : forall {value : Set}, set value.

  Parameter literal : forall {value : Set}, List.list value -> set value.

  Parameter mem : forall {value : Set}, value -> set value -> bool.

  Parameter cardinal : forall {value : Set}, set value -> Std.nat.

  Parameter add : forall {value : Set}, value -> set value -> set value.

  Parameter remove : forall {value : Set}, value -> set value -> set value.

  Parameter update : forall {a : Set}, a -> bool -> set a -> set a.

  Parameter iter : forall {a : Set}, (a -> unit) -> set a -> unit.

  Parameter fold : forall {accumulator item : Set},
    (accumulator * item -> accumulator) -> set item -> accumulator -> accumulator.

  Parameter fold_desc : forall {accumulator item : Set},
    (item * accumulator -> accumulator) -> set item -> accumulator -> accumulator.
End _Set_impl.
Include _Set_impl.
