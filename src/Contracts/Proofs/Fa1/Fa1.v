Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

From Hammer Require Import Tactics.
Require Import Lia.

Require CoqOfMligo.Result. Import Result.Notations.
Require CoqOfMligo.Virtual.Big_map.
Require CoqOfMligo.Virtual.Map.
Require CoqOfMligo.Virtual.Std.
Require CoqOfMligo.Virtual.Tezos.

Require CoqOfMligo.Proofs.Virtual.Big_map.
Require CoqOfMligo.Proofs.Virtual.Map.
Require CoqOfMligo.Proofs.Virtual.Std.
Require CoqOfMligo.Proofs.Virtual.Tezos.

Require CoqOfMligo.Contracts.Fa1.Fa1.

Module Account.
  Module Valid.
    Record t (a : Fa1.account) : Prop := {
        ac_balance : Std.Nat.Valid.t a.(Fa1.account.ac_balance);
        ac_allowances : Map.Valid.t Std.Nat.Valid.t a.(Fa1.account.ac_allowances);
      }.
  End Valid.
End Account.

Module Ledger.
  Module Valid.
    Definition t (l : Big_map.big_map Std.address Fa1.account) : Prop :=
      Big_map.Valid.t Account.Valid.t l.
  End Valid.
End Ledger.

Module Token_metadata.
  Module Valid.
    Definition t (tk : Big_map.big_map Std.nat (Std.nat * Map.map string bytes)) : Prop.
    Admitted.
  End Valid.
End Token_metadata.

Module Storage.
  Module Valid.
    Record t (s : Fa1.storage) : Prop := {
        ledger : Ledger.Valid.t s.(Fa1.storage.ledger);
        token_metadata : Token_metadata.Valid.t s.(Fa1.storage.token_metadata);
        supply : Std.Nat.Valid.t s.(Fa1.storage.supply);
      }.
  End Valid.
End Storage.

Module Transfer.
  Module Valid.
    Import Fa1.transfer.
    Record t (s : Fa1.transfer) : Prop := {
        tr_ammount : Std.Nat.Valid.t s.(tr_amount);
      }.
  End Valid.
End Transfer.

Module Approve.
  Module Valid.
    Import Fa1.approve.
    Record t (s : Fa1.approve) : Prop := {
      ap_value : Std.Nat.Valid.t s.(ap_value)
    }.
  End Valid.
End Approve.

Module Params.
  Module Valid.
    Definition t (p : Fa1.params) : Prop :=
      match p with
      | Fa1.Transfer t => Transfer.Valid.t t
      | Fa1.Approve a => Approve.Valid.t a
      end.
  End Valid.
End Params.

(* @TODO *)
Lemma tz1_is_valid :
  Tezos.Contract.Valid.t Fa1.tz1.
Admitted.

(* @TODO *)
Lemma initial_storage_is_valid :
  Storage.Valid.t Fa1.storage_value.
Admitted.

Module HasEnoughGas.
  Definition t s ac amount :=
    let ac := Fa1.get_account s ac in
    let balance := ac.(Fa1.account.ac_balance) in
    balance >= amount.
End HasEnoughGas.

Module HasEnoughAllowance.
  Definition t s t sd :=
    (Fa1.get_allowance
       (Fa1.get_account s t.(Fa1.transfer.tr_src)) sd -
       t.(Fa1.transfer.tr_amount)) >= 0.
End HasEnoughAllowance.

Module Helpers.
  (* @TODO implement [is_nat] and get rid of this *)
  Axiom is_nat_ge_0_eq :
    forall x,
    x >= 0 -> 
    Std.is_nat x = Some x.
End Helpers.

(** [get_account] always return a valid account *)
Lemma get_account_is_valid (s : Fa1.storage) (a : Std.address):
  Storage.Valid.t s ->
  Account.Valid.t (Fa1.get_account s a).
Proof.
  intros [HLedger _]. destruct s.
  unfold Fa1.get_account; simpl in *.
  unfold Ledger.Valid.t in HLedger.
  destruct (Big_map.find_opt _ _) eqn:?; [|now repeat constructor].
  now apply Big_map.find_opt_preserves_P
    with (domain_v := Account.Valid.t)
    in Heqo.
Qed.

(* @TODO *)
Lemma transfer_aux_is_valid (s : Fa1.storage) (sd : Std.address) (t : Fa1.transfer) :
  Transfer.Valid.t t ->
  Storage.Valid.t s ->
  HasEnoughGas.t s t.(Fa1.transfer.tr_src) t.(Fa1.transfer.tr_amount) ->
  HasEnoughAllowance.t s t sd ->
  letP? storage := Fa1.transfer_aux s sd t in
  Storage.Valid.t storage.
Proof.
  intros HTransfer HStorage HGas HAllow.
  unfold Fa1.transfer_aux.
  unfold HasEnoughAllowance.t, HasEnoughGas.t in * |-.
  repeat match goal with
  | |- context [Std.is_nat ?n] =>
      assert (HGas' : n >= 0) by lia
  end.
  rewrite Helpers.is_nat_ge_0_eq; [|easy].
  match goal with
  | |- context [Std.is_nat ?n] =>
      assert (HAllow' : n >= 0) by lia
  end.
  rewrite Helpers.is_nat_ge_0_eq; [|easy]; simpl;
  destruct (negb _) eqn:?, t; simpl in *;
  rewrite Std.Polymorphic_eq.polymorphic_eq_string_eq;
  repeat destruct (String.eqb _ _);
  match goal with 
  | H : negb true = true |- _ => discriminate H
  | H : negb false = false |- _ => discriminate H
  | H : negb false = true |- _ => clear H
  | H : negb true = false |- _ => clear H
  end; constructor; destruct s; simpl;
  repeat  match goal with 
  | |- Token_metadata.Valid.t _ => apply HStorage
  | |- Std.Nat.Valid.t _ => apply HStorage
  | |- Ledger.Valid.t _ => idtac
  end;
  unfold Ledger.Valid.t;
  match goal with
  | |- Big_map.Valid.t _ ?bm =>
      set (big_map := bm)
  end; unfold Big_map.Valid.t.
  (* I need to show that all accounts in the ledger (a big_map)
     are still valid. This in turn means to show that 
     all balances of all the accounts are not negative and 
     and all allowances (a map) values are also not negative.
   *)
  all : assert (TODO : Forall (fun '(_, account) => Account.Valid.t account) big_map)
   by admit; apply TODO.
Admitted.

(* @TODO *)
Lemma approve_aux_is_valid (s : Fa1.storage) (sd : Std.address) (a : Fa1.approve) :
  Approve.Valid.t a ->
  Storage.Valid.t s ->
  letP? storage := (Fa1.approve_aux s sd a) in
  Storage.Valid.t storage.
Admitted.

(* @TODO *)
Lemma transfer_value_is_valid (t : Fa1.transfer) (s : Fa1.storage) :
  Transfer.Valid.t t ->
  Storage.Valid.t s ->
  letP? '(_, storage) := Fa1.transfer_value (t, s) in
  Storage.Valid.t storage.
Admitted.

(* @TODO *)
Lemma approve_value_is_valid (a : Fa1.approve) (s : Fa1.storage) :
  Approve.Valid.t a ->
  Storage.Valid.t s -> 
  letP? '(_, storage) := Fa1.approve_value (a, s) in
  Storage.Valid.t storage.
Admitted.

(* @TODO *)
(** [Fa1.main] preserves the validity of the storage *)
Lemma main_is_valid (p : Fa1.params) (s : Fa1.storage) :
  Params.Valid.t p ->
  Storage.Valid.t s ->
  letP? '(_, storage) := Fa1.main (p, s) in
  Storage.Valid.t storage.
Admitted.

(** Initial context is valid *)
Lemma initial_context_is_valid : 
  Tezos.Context.Valid.t Fa1.context.
Proof.
  constructor; try easy; repeat constructor;
    match goal with 
    | |- context [Fa1.tz1] => apply tz1_is_valid
    | |- context [Tezos.mk_contract _] => apply Tezos.Contract.Valid.mk_contract
    | |- context [Tezos.mk_view _] => apply Tezos.View.Valid.mk_view
    end.
Qed.
