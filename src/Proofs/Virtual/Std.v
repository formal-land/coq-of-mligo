Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require CoqOfMligo.Virtual.Std.

From Hammer Require Import Tactics.

(* Move this to Proofs/Std.v *)
Module Nat.
  Module Valid.
    Definition t (n : Std.nat) := n >= 0.
  End Valid.
End Nat.

(* Move this to Proofs/Std.v *)
Module Address.
  Module Valid.
    Definition t (a : Std.address) : Prop.
      (* @TODO *)
    Admitted.
  End Valid.
End Address.

Module Polymorphic_eq.
  Module Valid.
    Record t {a a' : Set}
     {domain : a -> Prop} {f : a -> a'} {compare : a -> a -> int} : Prop := {
     congruence_left x1 x2 y :
       domain x1 -> domain x2 -> domain y ->
       f x1 = f x2 -> compare x1 y = compare x2 y;
     image x y :
       domain x -> domain y ->
       match compare x y with
       | -1 | 0 | 1 => True
       | _ => False
       end;
     zero x y :
       domain x -> domain y ->
       compare x y = 0 -> f x = f y;
     sym x y :
       domain x -> domain y ->
       compare x y = - compare y x;
     trans x y z :
       domain x -> domain y -> domain z ->
       compare x y = 1 ->
       compare y z = 1 ->
       compare x z = 1;
   }.
   Arguments t {_ _}.
  End Valid.

  Module Equal.
    Module Valid.
      Definition t {a : Set} (domain : a -> Prop) (equal : a -> a -> bool)
        : Prop :=
        forall x y,
          domain x -> domain y ->
          (x = y) <-> (equal x y = true).
      Lemma implies {a : Set} (domain1 domain2 : a -> Prop) equal :
        (forall x, domain2 x -> domain1 x) ->
        t domain1 equal ->
        t domain2 equal.
      Proof.
        unfold t; intros.
        hauto q:on.
      Qed.
    End Valid.
  End Equal.

  (** The polymorphic comparison is valid. *)
  Axiom polymorphic_compare_is_valid :
    forall {a : Set},
    Valid.t (fun _ => True) (fun x => x) (Std.polymorphic_compare (a := a)).

  Axiom polymorphic_eq_is_valid :
    forall {a : Set},
    Equal.Valid.t (fun _ => True) (Std.polymorphic_eq (a := a)).

  (** A useful lemma for tests using the polymorphic equality. *)
  Lemma polymorphic_eq_true : forall {a : Set} (v1 v2 : a),
    v1 = v2 ->
    Std.polymorphic_eq v1 v2 = true.
  Proof.
    intros.
    now apply (polymorphic_eq_is_valid (a := a)).
  Qed.

  Lemma polymorphic_eq_string_eq : 
    forall (a b : string),
      Std.polymorphic_eq a b = String.eqb a b.
  Proof.
    intros.
    pose proof (polymorphic_eq_is_valid a b) as Heq.
    simpl in Heq. specialize (Heq I I).
    destruct (Std.polymorphic_eq a b) eqn:?; symmetry.
    { apply String.eqb_eq. now rewrite Heq. }
    { destruct Heq as [Hneq _].
      assert (Hneq' : false = true <-> False) by sfirstorder.
      rewrite Hneq' in Hneq.
      rewrite String.eqb_neq.
      now unfold Logic.not.
    }
  Qed.
End Polymorphic_eq.
