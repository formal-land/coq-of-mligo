Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require CoqOfMligo.Virtual.Tezos.

Module Contract.
  Module Valid.
    Definition t {A B : Set} (c : Std.contract A B) : Prop.
      (* @TODO *)
    Admitted.
    
    (** [Tezos.mk_contract] return valid contracts *)
    Axiom mk_contract : forall {A B : Set} a b c d e f,
      @t string string (@Tezos.mk_contract A B a b c d e f).
  End Valid.
End Contract.

Module View.
  Module Valid.
    Definition t {A B C : Set} (v : Tezos.view A B C) : Prop.
      (* @TODO *)
    Admitted.

    (** [Tezos.mk_view] return valid views *)
    Axiom mk_view : forall {storage param res : Set} (a : storage) (b c : string) (d : param * storage -> res),
      let '(_, _, view) := Tezos.mk_view a b c d in
        @t string string string view.
  End Valid.
End View.

Module Context.
  Module Valid.
    Import Tezos.context.
    Record t (ctx : Tezos.context) : Prop := {
      source : ctx.(level) >= 0;
      time : ctx.(time) >= 0;
      amount : ctx.(amount) >= 0;
      contracts : List.Forall Contract.Valid.t ctx.(contracts);
      views : List.Forall (fun '(_, _, v) => View.Valid.t v) ctx.(views);
    }.
  End Valid.
End Context.
