Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require CoqOfMligo.Virtual.Map.

Module Valid.
  Definition t {key value : Set}
    (domain_v : value -> Prop) (m : Map.map key value) : Prop :=
    Forall (fun '(_, v) => domain_v v) m.
End Valid.

