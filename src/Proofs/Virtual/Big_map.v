Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

From Hammer Require Import Tactics.

Require CoqOfMligo.Virtual.Big_map.
(* @TODO 
   Generalize this to list and rewrite these
   lemmas over the [list], type so we can reuse them
   for Maps too *)
Module Valid.
  Definition t {key value : Set} 
    (domain_v : value -> Prop)
    (bm : Big_map.big_map key value) :=
    Forall (fun '(k, v) => domain_v v) bm.
End Valid.

(** Given a property of values [domain_v],
    that holds for every value in a [big_map]
    [Big_map.find_opt] preserves [domain_v] *)
Lemma find_opt_preserves_P :
  forall {key value : Set}
    (domain_v : value -> Prop)
    (k : key)
    (bm : Big_map.big_map key value)
    (v : value),
  Valid.t domain_v bm ->
  Big_map.find_opt k bm = Some v ->
  domain_v v.
Proof.
  intros.
  induction bm; sauto q: on.
Qed.
